package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	ch1, ch2 := make(chan string), make(chan string)

	go func() {
		time.Sleep(100 * time.Millisecond)
		ch1 <- "one"
	}()

	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- "two"
	}()

	/*
		done := make(chan struct{})
		go func() { // cancellation
			close(done)
		}()
	*/

	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()

	select {
	case v := <-ch1:
		fmt.Println("ch1:", v)
	case v := <-ch2:
		fmt.Println("ch2:", v)
	// case <-time.After(time.Millisecond):
	// case <-done:
	case <-ctx.Done():
		fmt.Println("timeout")
		/*
			default:
				fmt.Println("default")
		*/
	}
}
