package main

import "fmt"

func main() {
	var loc1 Location
	fmt.Println("loc1:", loc1)
	fmt.Printf("loc1  v: %v\n", loc1)
	fmt.Printf("loc1 +v: %+v\n", loc1)
	fmt.Printf("loc1 #v: %#v\n", loc1)

	loc2 := Location{1, 2}
	fmt.Printf("loc2: %#v\n", loc2)

	loc3 := Location{
		Y: 10,
		//		X: 5,
	}
	fmt.Printf("loc3: %#v\n", loc3)
	loc3.X = 100
	fmt.Printf("loc3: %#v\n", loc3)

	fmt.Println(NewLocation(100, 200))
	fmt.Println(NewLocation(100, 2000))

	loc3.Move(500, 500)
	fmt.Printf("loc3 (move): %#v\n", loc3)

	p1 := Player{
		Name:     "Parzival",
		Location: Location{100, 200},
	}
	fmt.Printf("p1: %#v\n", p1)
	fmt.Printf("p1.X: %#v\n", p1.X)
	p1.Move(200, 300)
	fmt.Printf("p1 (move): %#v\n", p1)

	fmt.Println(p1.Found(Jade))
	fmt.Println(p1.Found(Jade))
	fmt.Println(p1.Found(Key(21)))
	fmt.Println(p1.Keys)

	ms := []mover{
		&loc1,
		&p1,
	}
	moveAll(ms, 0, 0)
	for _, m := range ms {
		fmt.Println(m)
	}
}

/* Thought experiment: sorting interface */
type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}

/*
Interface in Go specify what we need (not what we provide)
Interfaces in Go tend to be small (stdlib avg less than 2, rule of thumb - no more that 5)
Rule of thumb: Accept interfaces, return types
*/

type mover interface {
	Move(int, int)
}

func moveAll(items []mover, x, y int) {
	for _, m := range items {
		m.Move(x, y)
	}
}

/* Exercise:
- Add a Keys field to Player which is []string
- Add a "Found(key string) error" method to player
	- It should err if key is not one of "jade", "copper", "crystal"
	- It should add a key only once to keys
		p1.Found("jade")
		p1.Found("jade")
		fmt.Println(p1.Keys) // [jade]
*/

func (p *Player) Found(key Key) error {
	if !validKey(key) {
		return fmt.Errorf("%q - invalid key", key)
	}

	if !p.hasKey(key) {
		p.Keys = append(p.Keys, key)
	}

	return nil
}

func (p *Player) hasKey(key Key) bool {
	for _, k := range p.Keys {
		if k == key {
			return true
		}
	}
	return false
}

func validKey(key Key) bool {
	// return key == Jade || key == Copper || key == Crystal
	return key >= Jade && key < maxKey
}

// String implement fmt.Stringer interface
func (k Key) String() string {
	switch k {
	case Jade:
		return "jade"
	case Copper:
		return "copper"
	case Crystal:
		return "crystal"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Key byte

const (
	Jade Key = iota + 1
	Copper
	Crystal
	maxKey // must be last
)

type Player struct {
	Name     string
	Location // Player embeds Location
	Keys     []Key
}

// Google: extending vs embedding in OO

// l is called "the receiver"
// Use pointer receiver when changing values
func (l *Location) Move(x, y int) {
	l.X = x
	l.Y = y
}

// func NewLocation(x, y int) Location
// func NewLocation(x, y int) (Location, error)
// func NewLocation(x, y int) *Location
func NewLocation(x, y int) (*Location, error) {
	if x < 0 || x > MaxX || y < 0 || y > MaxY {
		return nil, fmt.Errorf("%d/%d out of bounds for %d/%d", x, y, MaxX, MaxY)
	}

	// Go does escape analysis and will allocate loc on the heap
	loc := Location{X: x, Y: y}
	return &loc, nil
}

const (
	MaxX = 1000
	MaxY = 600
)

type Location struct {
	X int
	Y int
}
