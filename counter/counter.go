package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	/*
		var mu sync.Mutex
		counter := 0
	*/
	var counter int64

	n := 10
	var wg sync.WaitGroup
	wg.Add(n)
	for i := 0; i < n; i++ {
		go func() {
			defer wg.Done()
			for j := 0; j < 1_000; j++ {
				atomic.AddInt64(&counter, 1)

				/*
					mu.Lock()
					counter++
					mu.Unlock()
				*/

				time.Sleep(time.Millisecond)
			}
		}()
	}
	wg.Wait()
	fmt.Println(counter)
}
