package main

import (
	"errors"
	"fmt"
	"log"
	"os"
)

/*
	Opening files

read: os.Open
write: os.Create
append: os.OpenFile
*/

func main() {
	if err := Shutdown("app.pid"); err != nil {
		for e := err; e != nil; e = errors.Unwrap(e) {
			fmt.Printf("\t%s\n", e)
		}
		log.Fatalf("error: %s", err)
	}
}

func Shutdown(pidFile string) error {
	// Go idiom: acquire resource, check for error, defer release
	file, err := os.Open(pidFile)
	if err != nil {
		return err
	}
	defer file.Close()
	/*
		defer func() {
			if err := file.Close(); err != nil {
				// now what?
				log.Printf("warning: %q - can't close - %s", pidFile, err)
			}
		}()
	*/
	var pid int
	if _, err := fmt.Fscanf(file, "%d", &pid); err != nil {
		// %w will chain/wrap errors
		return fmt.Errorf("%q: bad pid - %w", pidFile, err)
	}

	// simulate shutdown
	fmt.Printf("shutting down %d\n", pid)
	if err := os.Remove(pidFile); err != nil {
		log.Printf("warning: can't delete %q - %s", pidFile, err)
	}
	return nil
}
