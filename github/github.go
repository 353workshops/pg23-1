package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

func main() {
	/*
			resp, err := http.Get("https://api.github.com/users/tebeka")
			if err != nil {
				log.Fatalf("ERROR: %s", err)
			}
			if resp.StatusCode != http.StatusOK {
				log.Fatalf("ERROR: bad status: %s", resp.Status)
			}

			// fmt.Println("content-type:", resp.Header.Get("Content-Type"))

			io.Copy(os.Stdout, resp.Body)
		// Goland: file, err := os.Open("github/response.json")
		file, err := os.Open("response.json")
		if err != nil {
			log.Fatalf("ERROR: %s", err)
		}
		fmt.Println(decode(file))
	*/
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	fmt.Println(githubInfo(ctx, "tebeka"))

}

type HTTPError int

func (e HTTPError) Error() string {
	return fmt.Sprintf("HTTP code %d", e)
}

func githubInfo(ctx context.Context, login string) (name string, public_repose int, err error) {
	// TODO: Consider URL encoding login
	url := fmt.Sprintf("https://api.github.com/users/%s", login)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return "", 0, err
	}

	// resp, err := http.Get(url)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", 0, err
	}

	if resp.StatusCode != http.StatusOK {
		// return "", 0, fmt.Errorf("%s: bad status - %s", url, resp.Status)
		return "", 0, HTTPError(resp.StatusCode)
	}

	return decode(resp.Body)
}

// decode decodes JSON response and return name and public_repos
type reply struct {
	Name     string
	NumRepos int `json:"public_repos"`
}

// map[string]any

func decode(r io.Reader) (string, int, error) {
	dec := json.NewDecoder(r)
	var re reply
	if err := dec.Decode(&re); err != nil {
		return "", 0, err
	}

	return re.Name, re.NumRepos, nil
}

/*
JSON <-> Go types
string <-> string
number <-> float64, float32, int, int8, int16, int32, int64, uint8 ...
array <-> []int, []string, ... []any
boolean <-> bool
object <-> struct, map[string]any
null <-> nil

encoding/json API
JSON -> []byte -> Go: Unmarshal
JSON -> io.Reader -> Go: Decoder
Go -> []byte -> JSON: Marshal
Go -> io.Writer -> JSON: Encoder
*/

/*
What printing does when you print v
is v an error? if so use v.Error()
is v an fmt.Stringer? if so use v.String()
is v an int ...
*/

// go run . | jq > response.json
