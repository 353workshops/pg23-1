package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

// What is the most common word (ignoring case) in sherlock.txt?
// - Split text to words ✓
// - Process only one line at a time ✓
// - Count words (word frequency)

var (
	// "Who's on first?" -> [Who s on first]
	wordRe = regexp.MustCompile(`[a-zA-Z]+`)
)

// `hello`: "raw" string, \ is just a \, you can write multi line strings

/*
func init() {
	fmt.Println("init")
}
*/

func main() {
	/*
		mapDemo()
		return
	*/

	/*
		matches := wordRe.FindAllString("Who's on first?", -1)
		fmt.Println(matches)
	*/

	file, err := os.Open("sherlock.txt")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	freq := make(map[string]int) // word -> count
	// var freq map[string]int
	for s.Scan() {
		for _, w := range wordRe.FindAllString(s.Text(), -1) {
			w = strings.ToLower(w)
			freq[w]++
		}
	}
	if err := s.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}

	maxW, maxC := "", 0
	for w, c := range freq {
		if c > maxC {
			maxW, maxC = w, c
		}
	}
	fmt.Println(maxW)
}

func mapDemo() {
	heros := map[string]string{
		"Clark": "Superman",
		"Bruce": "Batman",
	}
	// ww := heros["Diana"] // will return ""
	ww, ok := heros["Diana"]
	if ok {
		fmt.Printf("Diana: %q\n", ww)
	} else {
		fmt.Println("Diana not found")
	}
}
