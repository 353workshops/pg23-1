package stemmer_test

import (
	"fmt"

	"github.com/ardanlabs/nlp/stemmer"
)

func ExampleStem() {
	for _, w := range []string{"works", "working", "worked"} {
		fmt.Printf("%s -> %s\n", w, stemmer.Stem(w))
	}

	// Output:
	// works -> work
	// working -> work
	// worked -> work
}
