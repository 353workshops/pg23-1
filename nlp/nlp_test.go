package nlp

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestTokenize(t *testing.T) {
	// setup: call a function
	// teardown: defer or t.Cleanup
	text := "What's on second?"
	expected := []string{"what", "on", "second"}
	tokens := Tokenize(text)
	require.Equal(t, expected, tokens)
	/*
		if !reflect.DeepEqual(expected, tokens) {
			t.Fatalf("expected %#v, got %#v", expected, tokens)
		}
	*/
}

// Exercise: Read test cases from testdata/tokenize_cases.yml (instead of testCases slice)
type TokenizeCase struct {
	Text   string
	Tokens []string
	Name   string
}

func loadTokenizeCases(t *testing.T) []TokenizeCase {
	file, err := os.Open("testdata/tokenize_cases.yml")
	require.NoError(t, err, "open file")

	var testCases []TokenizeCase
	dec := yaml.NewDecoder(file)
	err = dec.Decode(&testCases)
	require.NoError(t, err, "decode YAML")

	return testCases
}

func TestTokenizeTable(t *testing.T) {
	/*
		testCases := []struct {
			text   string
			tokens []string
		}{
			{"", nil},
			{"Who's on first?", []string{"who", "s", "on", "first"}},
		}
	*/

	for _, tc := range loadTokenizeCases(t) {
		name := tc.Name
		if name == "" {
			name = tc.Text
		}
		t.Run(name, func(t *testing.T) {
			tokens := Tokenize(tc.Text)
			/*
				if !reflect.DeepEqual(tc.tokens, tokens) {
					t.Fatalf("expected %#v, got %#v", tc.tokens, tokens)
				}
			*/
			require.Equal(t, tc.Tokens, tokens)
		})
	}
}

/* Test function names
Test[_A-Z].*(t *testing.T) (TestTokenize, TestNLP, Test_stemmer ...)
Example[_A-Z].*():  (ExampleTokenize, Example_nlp ...)

Benchmark[_A-Z].*(b *testing.B) (BenchmarkTokenize ...)
Fuzz[_A-Z].*(f *testing.F) (FuzzTokenize ...)
*/
