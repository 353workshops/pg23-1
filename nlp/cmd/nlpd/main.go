package main

import (
	"context"
	"encoding/json"
	"expvar"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"

	"github.com/ardanlabs/nlp"
	"github.com/ardanlabs/nlp/stemmer"
)

type API struct {
	// TODO: logger, db connection ...
	srv         *http.Server
	logger      *log.Logger
	healthCalls *expvar.Int
}

func (a *API) healthHandler(w http.ResponseWriter, r *http.Request) {
	a.healthCalls.Add(1)
	a.logger.Printf("INFO: health called from %s", r.RemoteAddr)
	fmt.Fprintln(w, "OK")
}

func (a *API) quitHandler(w http.ResponseWriter, r *http.Request) {
	a.srv.Shutdown(context.Background())
}

// Exercise: Write tokenizeHandler that will read text from r.Body and
// return JSON with tokens

func (a *API) tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	// Step 1: Read, Parse & Validate data
	data, err := io.ReadAll(io.LimitReader(r.Body, 1_000_000))
	if err != nil {
		http.Error(w, "can't read", http.StatusBadRequest)
		return
	}

	// Step 2: Work
	text := string(data)
	tokens := nlp.Tokenize(text)

	// Step 3: Marshal output
	reply := map[string]any{
		"tokens": tokens,
	}

	data, err = json.Marshal(reply)
	if err != nil {
		http.Error(w, "can't marshal", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

// GET /stem/working
func (a *API) stemHandler(w http.ResponseWriter, r *http.Request) {
	word := chi.URLParam(r, "word")
	stem := stemmer.Stem(word)
	fmt.Fprintln(w, stem)
}

func main() {
	api := API{
		healthCalls: expvar.NewInt("health.calls"),
		logger:      log.New(log.Writer(), "[nlpd] ", log.LstdFlags|log.Lshortfile),
	}
	// mux := http.NewServeMux()
	mux := chi.NewRouter()
	mux.Get("/health", api.healthHandler)
	mux.Get("/quit", api.quitHandler)
	mux.Post("/tokenize", api.tokenizeHandler)
	mux.Get("/stem/{word}", api.stemHandler)
	mux.Handle("/debug/vars", expvar.Handler())

	srv := http.Server{
		Handler: mux,
		Addr:    ":8080",
	}
	api.srv = &srv

	if err := srv.ListenAndServe(); err != nil {
		log.Fatalf("error: %s", err)
	}
}

/* built-in routing
- if it ends with / - prefix math
- other exact match
*/
/*
	// default server
	http.HandleFunc("/health", s.healthHandler)


	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("error: %s", err)
	}
*/
