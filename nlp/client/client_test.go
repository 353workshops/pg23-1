package client

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

// See testify/mock for more advanced options
type errTripper struct{}

func (e errTripper) RoundTrip(*http.Request) (*http.Response, error) {
	return nil, fmt.Errorf("connection error")
}

func TestClient_Health(t *testing.T) {
	c := New("https://localhost:8080")
	c.c.Transport = errTripper{}
	err := c.Health(context.Background())
	require.Error(t, err)
}
