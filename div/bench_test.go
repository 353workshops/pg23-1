package main

import "testing"

func BenchmarkNoDefer(b *testing.B) {
	for i := 0; i < b.N; i++ {
		n := div(7, 3)
		if n != 2 {
			b.Fatal(n)
		}
	}
}

func BenchmarkDefer(b *testing.B) {
	for i := 0; i < b.N; i++ {
		n, _ := safeDiv(7, 3)
		if n != 2 {
			b.Fatal(n)
		}
	}
}
