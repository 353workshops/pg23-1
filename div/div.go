package main

import "fmt"

func main() {
	fmt.Println(safeDiv(7, 3))
	fmt.Println(safeDiv(7, 0))
	/*
		fmt.Println(div(7, 3))
		fmt.Println(div(7, 0))
	*/
}

func safeDiv(a, b int) (q int, err error) {
	// q & err are local variables in safeDiv
	defer func() {
		if e := recover(); e != nil {
			fmt.Printf("error: %s\n", e)
			err = fmt.Errorf("%s", e) // convert e (any) to error
		}
	}()

	return div(a, b), nil
	/* Miki don't like this style
	q = div(a, b)
	return
	*/
}

func div(a, b int) int {
	return a / b
}
