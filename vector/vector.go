package main

import "fmt"

// type constraints (generics) - go 1.18+

func main() {
	is := []int{2, 3, 1}
	// fmt.Println(MaxInts(is))
	fmt.Println(Max(is))

	fs := []float64{2, 3, 1}
	//fmt.Println(MaxFloat64s(fs))
	fmt.Println(Max(fs))
}

type Number interface {
	int | float64
}

func Max[T Number](values []T) (T, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("empty slice")
	}

	max := values[0]
	for _, n := range values[1:] {
		if n > max {
			max = n
		}
	}
	return max, nil
}

/*
func MaxInts(values []int) (int, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("empty slice")
	}

	max := values[0]
	for _, n := range values[1:] {
		if n > max {
			max = n
		}
	}
	return max, nil
}

func MaxFloat64s(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("empty slice")
	}

	max := values[0]
	for _, n := range values[1:] {
		if n > max {
			max = n
		}
	}
	return max, nil
}
*/
