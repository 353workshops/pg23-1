package main

import (
	"fmt"
	"unicode/utf8"
)

// banner("Go", 6)
//   Go
// ------

func main() {
	banner("Go", 6)
	banner("G☺", 6)
	s := "G☺"
	fmt.Println(s[0]) // byte
	// s[0] = 'g'        // strings are immutable

	a, b := 1, "1"
	// %d is a "verb"
	fmt.Printf("a=%d, b=%s\n", a, b)
	fmt.Printf("a=%04d, b=%s\n", a, b)
	fmt.Printf("a=%v, b=%v\n", a, b)
	fmt.Printf("a=%#v, b=%#v\n", a, b)
	// Use #v verb for logging & debugging
}

// numbers: int, uint, int8, int16, int32, int64, uint8 ...
// float32, float64
// Also complex & big
func banner(text string, width int) {
	// := means declare & assign
	// var offset int (offset will be 0)
	// offset = (width - len(text)) / 2
	// var offset int = (width - len(text)) / 2

	// len of string returns size in bytes
	// offset := (width - len(text)) / 2
	offset := (width - utf8.RuneCountInString(text)) / 2
	for i := 0; i < offset; i++ {
		fmt.Print(" ")
	}
	fmt.Println(text)
	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
}
