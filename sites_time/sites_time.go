package main

import (
	"io"
	"log"
	"net/http"
	"sync"
	"time"
)

func main() {
	urls := []string{
		"https://google.com",
		"https://go.dev",
		"https://httpbin.org/404",
	}

	var wg sync.WaitGroup
	wg.Add(len(urls))
	for _, url := range urls {
		url := url
		// wg.Add(1)
		go func() {
			defer wg.Done()
			siteTime(url)
		}()
	}
	wg.Wait() // blocks until all goroutines call wg.Done()
}

func siteTime(url string) {
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("error: %s - %s", url, err)
		return
	}

	if resp.StatusCode != http.StatusOK {
		log.Printf("error: %s - %s", url, resp.Status)
		return

	}

	// consume body
	if _, err := io.Copy(io.Discard, resp.Body); err != nil {
		log.Printf("error: %s - %s", url, err)
		return
	}

	duration := time.Since(start)
	log.Printf("info: %s - %v", url, duration)
}
