package main

import (
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 3; i++ {
		/* Solution 2: Use local loop variable */
		i := i // i will "shadow" i from line 12
		go func() {
			fmt.Println("go:", i)
		}()
		/* Solution 1: Use a parameter
		go func(n int) {
			fmt.Println("go:", n)
		}(i)
		*/
		/* BUG: All goroutines use the same i from the "for" loop
		go func() {
			fmt.Println("go:", i)
		}()
		*/
	}

	time.Sleep(10 * time.Millisecond)

	ch := make(chan int)
	go func() {
		ch <- 7 // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	fmt.Println(sleepSort([]int{7, 12, 5, 20, 9}))

	go func() {
		for i := 0; i < 3; i++ {
			ch <- i
		}
		close(ch)
	}()

	for n := range ch {
		fmt.Println("n:", n)
	}

	/* What range over ch does
	for {
		n, ok := <- ch
		if !ok {
			break
		}
		fmt.Println("n:", n)
	}
	*/

	val = <-ch
	fmt.Println("val (closed):", val)

	val, ok := <-ch
	fmt.Println("val (closed):", val, "ok:", ok)

	var ch2 chan string
	fmt.Println("ch2:", ch2)
}

/* Channel semantics
- send/receive will block until opposite operation (*)
	- buffered channel won't block first "n" sends
- receive from a closed channel will return 0 value without blocking
	- use "val, ok := <- ch"
- send to a closed channel panic
- closing a closed channel will panic
- send/receive to a nil channel will block forever
*/

/*
	sleep sort algorithm

for every value "n" in values, spin a goroutine that will
  - sleep "n" milliseconds
  - send "n" over a channel

collect all values form the channel to a slice and return it
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	for _, n := range values {
		n := n // avoid closure capture
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}

	var out []int
	for range values {
		n := <-ch
		out = append(out, n)
	}

	return out
}
