package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	p := Payment{
		From:   "Wile E. Coyote",
		To:     "ACME",
		Amount: 123.45,
	}
	p.Pay(time.Now().UTC())
	p.Pay(time.Now().UTC())
}

func (p *Payment) Pay(t time.Time) {
	p.once.Do(func() {
		p.pay(t)
	})
}

func (p *Payment) pay(t time.Time) {
	ts := t.Format(time.RFC3339)
	fmt.Printf("[%s] %s -> [$%.2f] -> %s\n", ts, p.From, p.Amount, p.To)
}

type Payment struct {
	From   string
	To     string
	Amount float64 // USD

	once sync.Once
}
