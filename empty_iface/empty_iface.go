package main

import "fmt"

func main() {
	// var i interface{}
	var i any // go 1.18+

	i = "Hi"
	fmt.Println(i)
	i = 7
	fmt.Println(i)
	// fmt.Println(i + 3) // won't compile

	n := i.(int) // type assertion
	fmt.Println("n:", n)

	// s := i.(string) // will panic
	s, ok := i.(string)
	if ok {
		fmt.Println("s:", s)
	} else {
		fmt.Println("not a string")
	}
}

// Rule of thumb: Don't use any (or interface{})
