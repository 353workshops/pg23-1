package main

import (
	"fmt"
	"sort"
)

func main() {
	var grades []int
	fmt.Printf("grades = %#v\n", grades)
	if grades == nil { // can use == on a slice only with nil
		fmt.Println("nil slice")
	}

	grades = []int{90, 84, 89, 76}
	fmt.Printf("grades = %#v\n", grades)
	fmt.Println("len:", len(grades))
	grades[1] = 91
	fmt.Printf("grades = %#v\n", grades)

	grades = append(grades, 82)
	fmt.Printf("grades = %#v\n", grades)

	first := grades[:3] // half-open range, up to but not including the last index
	fmt.Println("first:", first)
	first[0] = 50
	fmt.Println("grades:", grades, "first:", first)
	fmt.Printf("first: len=%d, cap=%d\n", len(first), cap(first))

	var s []int
	for v := 0; v < 100; v++ {
		s = appendInt(s, v)
	}
	fmt.Println(len(s), cap(s), s[:7])
	fmt.Println(concat([]string{"A", "B"}, []string{"C"}))

	vec := []float64{3, 1, 2}
	fmt.Println(median(vec)) // 2

	vec = []float64{3, 1, 2, 4}
	fmt.Println(median(vec)) // 2.5
	fmt.Println("vec:", vec)

	// Why append(s1, s2...) is problematic
	v := []string{"a", "b", "c", "d", "e", "f"}
	v1 := v[:3]
	v2 := []string{"X", "Y"}
	fmt.Println(concat(v1, v2))
	fmt.Println(v)

	// Addams Family
	family := []string{"Morticia", "Gomez", "Wednesday", "Pugsley"}
	// pointer semantics
	for i := range family { // indices
		fmt.Println(i)
		//		family[i] = "Itt"
	}

	for i, v := range family { // indices + values
		fmt.Println(i, "->", v)
	}

	// value semantics
	for _, v := range family { // values
		//		family = append(family, "Lark")
		fmt.Println(v)
		// v = "Itt"
	}
	// fmt.Println(family)

	// arr is an array
	arr := [3]int{1, 2, 3}
	fmt.Printf("arr: %v (%T)\n", arr, arr)

}

func median(vec []float64) float64 {
	// Copy in order not to mutate vec
	vec2 := make([]float64, len(vec))
	copy(vec2, vec)

	sort.Float64s(vec2)
	i := len(vec2) / 2
	if len(vec2)%2 == 1 {
		return vec2[i]
	}
	return (vec2[i-1] + vec2[i]) / 2
}

// concat concatenates two slices
// concat([]string{"A", "B"}, []string{"C"}) -> []string{"A", "B", "C"}
func concat(s1, s2 []string) []string {
	// TODO: No "for" loops
	//return append(s1, s2...) // can override values in s1 underlying array
	s := make([]string, len(s1)+len(s2))
	copy(s, s1)
	copy(s[len(s1):], s2)
	return s
}

func appendInt(s []int, v int) []int {
	i := len(s)
	if len(s) < cap(s) { // enough space in the underlying array
		s = s[:len(s)+1]
	} else { // need to re-allocate & copy
		size := 2*len(s) + 1
		fmt.Println(len(s), "->", size)
		s1 := make([]int, size)
		copy(s1, s)
		s = s1[:len(s)+1]
	}
	s[i] = v
	return s
}
